<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<title><?php
			/*
			 * Print the <title> tag based on what is being viewed.
			 * We filter the output of wp_title() a bit -- see
			 * boilerplate_filter_wp_title() in functions.php.
			 */
			wp_title( '|', true, 'right' );
		?></title>
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style.css" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/rslides.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/scripts.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/selectbox.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,400italic,700,700italic%7CCabin:400,400italic,600,600italic' rel='stylesheet' type='text/css'>


<?php
		/* We add some JavaScript to pages with the comment form
		 * to support sites with threaded comments (when in use).
		 */
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

		/* Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head();
?>
	</head>
	<body <?php body_class(); ?>>

<div id="container">

	<div id="main" class="clearfix">
		
        <div id="header">
        	
            <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" class="logo" /></a>
            
            <div class="top-box">
           	

				<?php 
					wp_nav_menu( array(
						'theme_location' => 'top',
						'container' => false,
						'items_wrap' => '<ul id="menu-top-navigation" class="menu">%3$s</ul>',
						'depth' => 1
					)); 
				?>

				<div class="recovery">
					<?php if ( !function_exists('dynamic_sidebar') ||
					           !dynamic_sidebar('Top Box') ) : ?>
					  <!-- This will be displayed if the sidebar is empty -->
					<?php endif; ?>
	            </div>  
            </div><!-- end .top-box -->
            
        	<nav id="access" role="navigation">

        		<?php 
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'container' => false,
						'items_wrap' => '<ul id="menu-main-navigation" class="menu">%3$s</ul>',
						'depth' => 0
					)); 
				?>
			</nav>
        </div>

