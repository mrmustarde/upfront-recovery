<div id="left">

<?php
	$parent_title = get_the_title($post->post_parent);
	$parent_link = get_permalink($post->post_parent);

	if($post->post_parent)
	  	$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
	else
	  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
?>
	<ul class="subpage-menu">
		<h2 class="widget-title">
			<a href="<?php echo $parent_link; ?>" ><?php echo $parent_title; ?></a>
		</h2>

		<?php 
			if ($children) {
				echo $children; 
			}
		?>
	</ul>

</div> <!-- end #left -->