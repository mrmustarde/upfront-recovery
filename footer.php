<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>

            </div><!-- #main -->
            </div><!-- #container -->

	<div id="footer">
	<div class="footer-content">
		<?php 
			wp_nav_menu( array(
				'theme_location' => 'footer-social',
				'container' => false,
				'items_wrap' => '<ul id="menu-social-navigation" class="menu">%3$s</ul>',
				'depth' => 1,
				'walker' => new description_to_js()
			)); 


		?>


            <div id="footer-left">
            	<h1>From Our Clients</h1>
                                	<div class="testimonial footer">
                    	<p>&#8220;I owe the full life I have to the treatment team and the women who became my sisters.&#8221;&nbsp;&nbsp;<a class="excerpt-link" href="testimonials/caitlin-g/index.html" >Read More</a></p>
                                                
                    </div>
                    <a href="testimonials/index.html" class="button">View All</a>
                            </div>
            
            <div id="footer-right">
            	
            	<h1>Get Help Now</h1><link rel='stylesheet' id='gforms_css-css'  href='wp-content/plugins/gravityforms/css/forms-ver=3.4.2.css' type='text/css' media='all' />
<script>
jQuery(function(){
jQuery('#input_1_1').attr('placeholder','Full Name');
jQuery('#input_1_2').attr('placeholder','Email Address');
jQuery('#input_1_5').attr('placeholder','Phone Number');
jQuery('#input_1_9').attr('placeholder','How did you hear about us?');
jQuery('#input_1_4').attr('placeholder','Notes');
});
</script>

                <div class='gf_browser_unknown gform_wrapper' id='gform_wrapper_1' ><form method='post' enctype='multipart/form-data'  id='gform_1'  action='/'>
                        <div class='gform_body'>
                            <ul id='gform_fields_1' class='gform_fields top_label description_below'><li id='field_1_7' class='gfield      gfield_html  gfield_no_follows_desc' ></li><!-- close the html field li -->
</ul><!-- close the list -->
<div id="form_left">
<ul class="gform_fields top_label description_below"></li><li id='field_1_1' class='gfield' ><label class='gfield_label' for='input_1_1'>Name</label><div class='ginput_container'><input name='input_1' id='input_1_1' type='text' value='' class='large' tabindex='1' /></div></li><li id='field_1_2' class='gfield' ><label class='gfield_label' for='input_1_2'>Email</label><div class='ginput_container'><input name='input_2' id='input_1_2' type='text' value='' class='large'  tabindex='2'   /></div></li><li id='field_1_5' class='gfield' ><label class='gfield_label' for='input_1_5'>Phone</label><div class='ginput_container'><input name='input_5' id='input_1_5' type='text' value='' class='large' tabindex='3'  /></div></li><li id='field_1_6' class='gfield      gfield_html  gfield_no_follows_desc' ></li>
</ul>
</div>
<div id="form_right">
<ul class="gform_fields top_label description_below"></li><li id='field_1_9' class='gfield' ><label class='gfield_label' for='input_1_9'>How did you hear about us?</label><div class='ginput_container'><input name='input_9' id='input_1_9' type='text' value='' class='large'  tabindex='4'   /></div></li><li id='field_1_4' class='gfield' ><label class='gfield_label' for='input_1_4'>Questions / Comments</label><div class='ginput_container'><textarea name='input_4' id='input_1_4' class='textarea medium' tabindex='5'   rows='10' cols='50'></textarea></div></li><li id='field_1_8' class='gfield      gfield_html  gfield_no_follows_desc' ></li><!-- close the html field li -->
</ul><!-- close the list -->
</div><!-- close the third div --></li>
                            </ul></div>
        <div class='gform_footer top_label'> <input type='submit' id='gform_submit_button_1' class='button gform_button' value='Submit' tabindex='6' />
            <input type='hidden' class='gform_hidden' name='is_submit_1' value='1' />
            <input type='hidden' class='gform_hidden' name='gform_submit' value='1' />
            <input type='hidden' class='gform_hidden' name='gform_unique_id' value='507107126c2fe' />
            <input type='hidden' class='gform_hidden' name='state_1' value='YToyOntpOjA7czo2OiJhOjA6e30iO2k6MTtzOjMyOiJiMzliNDZlNjY5NmIwMjczNTI4YmM4YWVjNWVmODUxNSI7fQ==' />
            <input type='hidden' class='gform_hidden' name='gform_target_page_number_1' id='gform_target_page_number_1' value='0' />
            <input type='hidden' class='gform_hidden' name='gform_source_page_number_1' id='gform_source_page_number_1' value='1' />
            <input type='hidden' name='gform_field_values' value='' />
            
        </div>
                </form>
                </div><script type='text/javascript'> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } ); </script>            </div><br class="clear" />



            
            <div id="copyright">

				<?php 
					wp_nav_menu( array(
						'theme_location' => 'footer',
						'container' => false,
						'items_wrap' => '<ul id="menu-footer-navigation" class="menu">%3$s</ul>',
						'depth' => 1
					)); 
				?>

				<?php if ( !function_exists('dynamic_sidebar') ||
           			!dynamic_sidebar('Footer Copyright/Contact') ) : ?>
  					<!-- This will be displayed if the sidebar is empty -->
				<?php endif; ?>
            </div><!-- #copyright -->
    </div>
</div>

<script type='text/javascript' src='wp-content/uploads/shadowbox-js/8796d7dfbaeb3fd2a5f6f47c6f40b66c.js?ver=3.0.3'></script>

<!-- Begin Shadowbox JS v3.0.3.9 -->
<!-- Selected Players: html, iframe, img, qt, swf, wmp -->
<script type="text/javascript">
/* <![CDATA[ */
	var shadowbox_conf = {
		animate: true,
		animateFade: true,
		animSequence: "sync",
		modal: false,
		showOverlay: true,
		overlayColor: "#ffffff",
		overlayOpacity: "0.9",
		flashBgColor: "#000000",
		autoplayMovies: true,
		showMovieControls: true,
		slideshowDelay: 0,
		resizeDuration: "0.35",
		fadeDuration: "0.35",
		displayNav: true,
		continuous: false,
		displayCounter: true,
		counterType: "default",
		counterLimit: "10",
		viewportPadding: "20",
		handleOversize: "resize",
		handleUnsupported: "link",
		autoDimensions: false,
		initialHeight: "160",
		initialWidth: "320",
		enableKeys: true,
		skipSetup: false,
		useSizzle: false,
		flashParams: {bgcolor:"#000000", allowFullScreen:true},
		flashVars: {},
		flashVersion: "9.0.0"
	};
	Shadowbox.init(shadowbox_conf);
/* ]]> */
</script>

<!-- End Shadowbox JS -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
	</body>
</html>