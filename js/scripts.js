jQuery.noConflict();
  jQuery(document).ready(function($){


function activatePlaceholders() {
var detect = navigator.userAgent.toLowerCase();
if (detect.indexOf("safari") > 0) return false;
var inputs = document.getElementsByTagName("input");
for (var i=0;i<inputs.length;i++) {
  if (inputs[i].getAttribute("type") == "text") {
   if (inputs[i].getAttribute("placeholder") && inputs[i].getAttribute("placeholder").length > 0) {
    inputs[i].value = inputs[i].getAttribute("placeholder");
    inputs[i].onclick = function() {
     if (this.value == this.getAttribute("placeholder")) {
      this.value = "";
     }
     return false;
    }
    inputs[i].onblur = function() {
     if (this.value.length < 1) {
      this.value = this.getAttribute("placeholder");
     }
    }
   }
  }
}
}
window.onload=function() {
activatePlaceholders();
}

  $(function () {
   $(".rslides").responsiveSlides({
  auto: true,             // Boolean: Animate automatically, true or false
  speed: 1300,            // Integer: Speed of the transition, in milliseconds
  timeout: 5000,          // Integer: Time between slide transitions, in milliseconds
  pager: true,           // Boolean: Show pager, true or false
  nav: true,             // Boolean: Show navigation, true or false
  random: false,          // Boolean: Randomize the order of the slides, true or false
  pause: false,           // Boolean: Pause on hover, true or false
  pauseControls: false,   // Boolean: Pause when hovering controls, true or false
  prevText: "Previous",   // String: Text for the "previous" button
  nextText: "Next",       // String: Text for the "next" button
  maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
  controls: "",           // Selector: Where controls should be appended to, default is after the 'ul'
  namespace: "rslides",   // String: change the default namespace used
  before: function(){$("." + this.namespace + "1_on div.banner-content").animate({left: "0",opacity:"hide"}, "slow");},   // Function: Before callback
  after: function(){$("." + this.namespace + "1_on div.banner-content").animate({left: "-50px",opacity:"show"}, "slow");}   // Function: After callback
});

  });
 
  $(function () {
   $(".staff-rotator").responsiveSlides({
  auto: true,             // Boolean: Animate automatically, true or false
  speed: 600,            // Integer: Speed of the transition, in milliseconds
  timeout: 12000,          // Integer: Time between slide transitions, in milliseconds
  pager: false,           // Boolean: Show pager, true or false
  nav: true,             // Boolean: Show navigation, true or false
  random: false,          // Boolean: Randomize the order of the slides, true or false
  pause: false,           // Boolean: Pause on hover, true or false
  pauseControls: false,   // Boolean: Pause when hovering controls, true or false
  prevText: "Previous",   // String: Text for the "previous" button
  nextText: "Next",       // String: Text for the "next" button
  maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
  controls: "",           // Selector: Where controls should be appended to, default is after the 'ul'
  namespace: "rslides",   // String: change the default namespace used
  before: function(){},   // Function: Before callback
  after: function(){}   // Function: After callback
});

  });

$("a.rslides_nav").css({opacity: "0"});

$(".top-level > a").removeAttr("href").css("cursor","pointer");


$("ul#menu-social-navigation li").hover(function(){
    $(this).animate({top: "-5px"}, "fast");
},
function(){
    $(this).animate({top: "0px"}, "fast");
});


$("a.slider").hover(function(){
    $(this).animate({paddingLeft: '10px'}, "fast");
},
function(){
    $(this).animate({paddingLeft: '0px'}, "fast");
});

$(".toggle_container").hide();

$("h2.trigger").toggle(function(){
$(this).addClass("active");
}, function () {
$(this).removeClass("active");
});

$("h2.trigger").click (function() {
$(this).next('.toggle_container').toggle(450);
return true;
});

$("ul#menu-main-navigation li").hover(function(){
    $(this).find("ul.sub-menu").animate({opacity: "show"}, 350);
},
function(){
    $(this).find("ul.sub-menu").animate({opacity: "hide"}, 350);
});

$("a.rslides_nav").hover(function(){
    $(this).animate({opacity: "1"}, "fast");
},
function(){
    $(this).animate({opacity: "0.5"}, "fast");
});

$("ul.staff-page li").hover(function(){
    $(this).find('div.fader').animate({opacity: "show"}, 250);
},
function(){
    $(this).find('div.fader').animate({opacity: "hide"}, 250);
});

$("#rotator-wrap").hover(function(){
    $("a.rslides1_nav").animate({opacity: "0.5"}, 350);
},
function(){
    $("a.rslides1_nav").animate({opacity: "0"}, 350);
});

$("#staff-wrap").hover(function(){
    $("a.rslides2_nav").animate({opacity: "0.5"}, 350);
},
function(){
    $("a.rslides2_nav").animate({opacity: "0"}, 350);
});

$("ul#menu-main-navigation li:last-child").addClass("last");

$("div.opacity").css({opacity: "0.8"});

$("#footer-right select").selectBox();

$("#footer-right select").addClass("selectBox-dropdown");

$(function(){

    $('a[href*=#]').click(function() {
    
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        && location.hostname == this.hostname) {
        
            var $target = $(this.hash);
            
            $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
            
            if ($target.length) {
            
                var targetOffset = $target.offset().top;
                
                $('html,body').animate({scrollTop: targetOffset}, 600);
                    
                return false;
                
            }
            
        }
        
    });
    
});

});