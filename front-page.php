<?php
/**
 *
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 *
 * Template Name: Front Page
 *
 */

get_header(); ?>



        <div id="content" role="main">


            	<div id="banner">
                	<div id="rotator-wrap">
                    <ul id="rotator" class="rslides">
                                	<li style="background:url(http://balboahorizons.com/wp-content/uploads/2012/07/banner-01.jpg) no-repeat 0 0;">
                        <div class="swoop"></div>                        <div class="banner-content">
                        	<h1>Inspiring Change. Transforming Lives.</h1>
                        	<p>Balboa Horizons offers the full continuum of gender specific addiction recovery programs for men and women. We specialize in the treatment of alcohol and drug addiction along with co-occurring mental health conditions or dual diagnosis. Our ability to treat clients in every stage of recovery provides invaluable continuity of care.</p>
                        	<a class="button" href="about-us/index.html">Learn More</a>
                        </div>                 </li>
                        	<li style="background:url(http://balboahorizons.com/wp-content/uploads/2012/08/oceanwithviewer.jpg) no-repeat 0 0;">
                        <div class="swoop"></div>                        <div class="banner-content light">
                        	<h1>Opiate Addiction Treatment</h1>
                        	<p>The rising popularity of prescription pain medications has made opioid addiction an alarming epidemic. Balboa Horizons’ clinical team is highly trained to deal with the challenges involved with opioid addiction and treatment.</p>
                        	<a class="button" href="opiate-addiction-treatment/index.html">Learn More</a>
                        </div>                 </li>
                        	<li style="background:url(http://balboahorizons.com/wp-content/uploads/2012/08/beackwalk.jpg) no-repeat 0 0;">
                                                <div class="banner-content">
                        	<h1>Women&#8217;s Addiction Treatment</h1>
                        	<p>Our program allows for an individualized treatment process that fosters maximum progress and growth.The women at Balboa Horizons graduate from our facility with happiness, confidence and, most importantly, a foundation for continued sobriety. Their success is our success.</p>
                        	<a class="button" href="programs/womens-addiction-treatment/index.html">Learn More</a>
                        </div>                 </li>
                        	<li style="background:url(http://balboahorizons.com/wp-content/uploads/2012/07/banner-06.jpg) no-repeat 0 0;">
                        <div class="swoop"></div>                        <div class="banner-content">
                        	<h1>Men&#8217;s Addiction Treatment</h1>
                        	<p>Our program addresses addiction along with co-occurring mental health diagnoses commonly found in men. This means that we are adept at addressing the addiction or drinking issues <em>and </em>any mild to moderate mental and emotional health issues.</p>
                        	<a class="button" href="programs/mens-addiction-treatment/index.html">Learn More</a>
                        </div>                 </li>
                        	<li style="background:url(http://balboahorizons.com/wp-content/uploads/2012/07/banner-04.jpg) no-repeat 0 0;">
                        <div class="swoop"></div>                        <div class="banner-content light">
                        	<h1>Family Program</h1>
                        	<p>Family therapy at Balboa Horizons focuses on more than just building a support structure to the client we also highlight the importance of supporting family members in their own process of healing and recovery from the affects the addiction had on each of them.</p>
                        	<a class="button" href="admissions/for-families/index.html">Learn More</a>
                        </div>                 </li>
                        	<li style="background:url(http://balboahorizons.com/wp-content/uploads/2012/07/banner-08.jpg) no-repeat 0 0;">
                        <div class="swoop"></div>                        <div class="banner-content">
                        	<h1>College Program</h1>
                        	<p>The Future Horizons College Program balances and enhances recovery with educational endeavors. We are proud to offer the most innovative program for students looking to begin or continue their education while learning to sustain a sober lifestyle.</p>
                        	<a class="button" href="programs/future-horizons-collegeprogram/index.html">Learn More</a>
                        </div>                 </li>
                                </ul>
                    </div>
                    
                    <div id="banner-navigation">
                    	<ul id="menu-program-navigation" class="menu"><li id="menu-item-207" class="for-women menu-item menu-item-type-post_type menu-item-object-page"><h1><span class="lowercase">For</span><span class="uppercase">Women</span></h1><p class="description">Our program allows for an individualized treatment process that fosters maximum progress and growth.</p><a href="programs/womens-addiction-treatment/index.html"  class="button">Learn More</a></li>
<li id="menu-item-208" class="for-men menu-item menu-item-type-post_type menu-item-object-page"><h1><span class="lowercase">For</span><span class="uppercase">Men</span></h1><p class="description">Our program addresses addiction along with co-occurring mental health diagnoses commonly found in men.</p><a href="programs/mens-addiction-treatment/index.html"  class="button">Learn More</a></li>
<li id="menu-item-209" class="for-families menu-item menu-item-type-post_type menu-item-object-page"><h1><span class="lowercase">For</span><span class="uppercase">Families</span></h1><p class="description">We help families learn how to improve communication and support loved ones in treatment.</p><a href="admissions/for-families/index.html"  class="button">Learn More</a></li>
<li id="menu-item-210" class="for-alumni menu-item menu-item-type-post_type menu-item-object-page"><h1><span class="lowercase">For</span><span class="uppercase">Alumni</span></h1><p class="description">At Balboa Horizons, your treatment is just the first step on an exciting lifelong journey of recovery.</p><a href="alumni/index.html"  class="button">Learn More</a></li>
<li id="menu-item-206" class="for-students menu-item menu-item-type-post_type menu-item-object-page"><h1><span class="lowercase">For</span><span class="uppercase">Students</span></h1><p class="description">The Future Horizons College Program balances and enhances recovery with educational endeavors.</p><a href="programs/future-horizons-collegeprogram/index.html"  class="button">Learn More</a></li>
</ul>                    </div>
                    
                </div>
				
                <div id="action">
                
                	<div id="action-left">
                	
                    	<h2 class="action news">Latest News</h2>
	                	<?php 
	                		$args = array(
							    'numberposts' => 2,
							    'orderby' => 'post_date',
							    'order' => 'DESC',
							    'post_type' => 'post',
							    'post_status' => 'publish'
							); 
						?>
                	
	                	<?php 
                			$recent_posts = wp_get_recent_posts($args);
                			foreach( $recent_posts as $recent ){
	                			$the_post = get_post($recent["ID"]);
                			
                		?>
                		
                			<div class="blogitem">
                            	<div class="blog-thumb">
                                    <a href="<?php echo get_permalink($recent["ID"]); ?>">
                                    	<?php echo get_the_post_thumbnail($recent["ID"], 'thumbnail');?>
                                    </a>
								</div>
                                <div class="blog-content">
                                	<h2 class="blogtitle">
                                		<?php echo get_the_title($recent["ID"]);?>
                                	</h2>
                                    <h3 class="date">
                                    	<?php 
                                    		$time =  get_the_time('', $recent["ID"]); 
                                    		$date =  get_the_date('', $recent["ID"]); 
                                    		echo $date . '&nbsp;&nbsp;|&nbsp;&nbsp;' . $time;
                                    	?>
                                    </h3>
                					<p>
                						<?php echo substr($the_post->post_content, 0, 200); ?>
                						... <a class="excerpt-link" href="<?php echo get_permalink($recent["ID"]); ?>" >Read More</a>

                					</p>
                                </div><br class="clear" />
                            </div>            		
                		<?php } ?>	                	
                	</div><!-- end #action-left -->
	        
	                            
                    <div id="action-right">
                    	<a id="opiates" href="opiate-addiction-program-at-balboa-horizons/index.html">Addicted<br /><span class="lowercase">to</span> Opiates?</a>
                    	<h2 class="action staff">Our Staff</h2>
                        <div id="staff-wrap">
                        <ul class="staff-rotator">
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/paul-moen/index.html"><img width="140" height="176" src="wp-content/uploads/2012/08/Paul-Moen-4_web-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="Paul Moen 4_web" title="Paul Moen 4_web" /></a></div>
                                <h2 class="blogtitle small">Paul Moen</h2>
								<h3 class="date">CEO</h3>
                                <p> A 20-year veteran of the recovery community, Moen incorporates the principles of his personal recovery into his professional role as CEO. &nbsp;&nbsp;<a class="excerpt-link" href="staff/paul-moen/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/deb-hughes-cas/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/deb-hughes-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="deb-hughes" title="deb-hughes" /></a></div>
                                <h2 class="blogtitle small">Deb Hughes, CAS</h2>
								<h3 class="date">Executive Director</h3>
                                <p>As Executive Director, Deb applies her 27 years of experience in the field of addictions and eating disorders in all capacities of treatment.&nbsp;&nbsp;<a class="excerpt-link" href="staff/deb-hughes-cas/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><img src="wp-content/themes/Balboa/images/no-photo.jpg" height="176" width="140" /></a></div>
                                <h2 class="blogtitle small">Brooke A. Denni, MA, LMFT</h2>
								<h3 class="date">Clinical Director</h3>
                                <p>As Clinical Director at Balboa Horizons, Brooke works closely with the executive director in program development, management and supervision of clinical activities. Brooke ensures that client care meets and exceeds industry standards. &nbsp;&nbsp;<a class="excerpt-link" href="staff/brooke-a-denni-ma-lmft/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/amy-field-lmft-cas/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/amy-field-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="amy-field" title="amy-field" /></a></div>
                                <h2 class="blogtitle small">Amy Field, LMFT, CAS</h2>
								<h3 class="date">Primary Therapist </h3>
                                <p>Amy has worked in the addiction recovery field for over 25 years. She is a licensed Marriage and Family Therapist and Certified Addiction Specialist.&nbsp;&nbsp;<a class="excerpt-link" href="staff/amy-field-lmft-cas/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/betsy-bradley-cadc-ii/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/betsy-bradley-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="betsy-bradley" title="betsy-bradley" /></a></div>
                                <h2 class="blogtitle small">Betsy Bradley, CADC-II</h2>
								<h3 class="date">Drug & Alcohol Counselor</h3>
                                <p>Betsy has 14 years of experience working in addiction treatment. Her approach is eclectic and based in the Family Systems Theory and cognitive behavioral techniques.&nbsp;&nbsp;<a class="excerpt-link" href="staff/betsy-bradley-cadc-ii/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/linda-kraemer-lmft/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/linda-kraemer-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="linda-kraemer" title="linda-kraemer" /></a></div>
                                <h2 class="blogtitle small">Linda Kraemer, LMFT</h2>
								<h3 class="date">Therapist</h3>
                                <p>Linda is a Licensed Marriage and Family Therapist who specializes in Dialectical Behavioral Therapy, or DBT.&nbsp;&nbsp;<a class="excerpt-link" href="staff/linda-kraemer-lmft/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/christina-huxford-ctp/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/christina-huxford-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="christina-huxford" title="christina-huxford" /></a></div>
                                <h2 class="blogtitle small">Christina Huxford, CTP</h2>
								<h3 class="date">Trauma & Experiential Therapy Specialist</h3>
                                <p>As a Certified Trauma Professional and Reiki Master, Christina is certified in Somatic Trauma Resolution.&nbsp;&nbsp;<a class="excerpt-link" href="staff/christina-huxford-ctp/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><img src="wp-content/themes/Balboa/images/no-photo.jpg" height="176" width="140" /></a></div>
                                <h2 class="blogtitle small">Patrick Smith</h2>
								<h3 class="date">Drug & Alcohol Counselor </h3>
                                <p>Patrick is a drug and alcohol counselor at Balboa Horizons. &nbsp;&nbsp;<a class="excerpt-link" href="staff/patrick-smith/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/christine-carey/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/christine-carey-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="christine-carey" title="christine-carey" /></a></div>
                                <h2 class="blogtitle small">Christine Carey</h2>
								<h3 class="date">Group Facilitator</h3>
                                <p>Christine has been involved in teaching and guidance in the recovery field for over 20 years. She produced the television series Solutions, and was writer and editor of the accompanying magazine of the same name.&nbsp;&nbsp;<a class="excerpt-link" href="staff/christine-carey/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                	<li>
                            	<div class="staff-thumb"><a href="staff/caitlin-murashie-cadca-rrw/index.html"><img width="140" height="176" src="wp-content/uploads/2012/07/caitlin-murashie-140x176.jpg" class="attachment-staff-thumbnail-sm wp-post-image" alt="caitlin-murashie" title="caitlin-murashie" /></a></div>
                                <h2 class="blogtitle small">Caitlin Murashie, CADCA, RRW</h2>
								<h3 class="date">Case Manager </h3>
                                <p>Caitlin is a Drug and Alcohol Counselor and Registered Recovery Worker. She has worked in a variety of treatment programs including adolescent and detox facilities in Orange County.&nbsp;&nbsp;<a class="excerpt-link" href="staff/caitlin-murashie-cadca-rrw/index.html" >Read More</a></p>
                                <a href="meet-our-team/index.html" class="button">See All Staff</a>
                            </li>
                                                </ul>
                        
                        
                        
                        </div>
                    </div>
                    <br class="clear" />
				
				</div>
                
              
                
			</div><!-- #content -->

<?php
	get_footer(); 
?>