<?php get_header(); ?>


<div id="content" role="main" class="subpage">
				
    <div id="sub-content">
    
    <?php get_template_part( 'sidebar', 'menu'); ?> 

<div id="right">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<h1 class="pagetitle"><?php the_title(); ?></h1>
		<?php the_content(); ?>
	<?php 	endwhile; ?>
    
</div><!-- end #right -->            
<br class="clear" />
</div><!--#sub-content-->    
</div><!-- #content -->

<?php get_footer(); ?>